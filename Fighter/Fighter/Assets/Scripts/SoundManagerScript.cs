using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip BgMusic,
        SoraJumpSound, SoraAttackSound1, SoraAttackSound2, SoraAttackSound3,SoraJump1,
        GokuKame, GokuJump1, GokuAttack1,
        Hurt1,Hurt2,Hurt3;
    static AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        SoraJumpSound = Resources.Load<AudioClip>("SoraJump");
        SoraAttackSound1 = Resources.Load<AudioClip>("SoraAttack1");
        SoraAttackSound2 = Resources.Load<AudioClip>("SoraAttack2");
        SoraAttackSound3 = Resources.Load<AudioClip>("SoraAttack3");
        SoraJump1 = Resources.Load<AudioClip>("SoraJump1");

        GokuKame = Resources.Load<AudioClip>("GokuKame");
        GokuJump1 = Resources.Load<AudioClip>("GokuJump1");
        GokuAttack1 = Resources.Load<AudioClip>("GokuAttack1");


        BgMusic = Resources.Load<AudioClip>("BgMusic");
        Hurt1 = Resources.Load<AudioClip>("Hurt1");
        Hurt2 = Resources.Load<AudioClip>("Hurt2");
        Hurt3 = Resources.Load<AudioClip>("Hurt3");

        audioSrc = GetComponent<AudioSource>();
        audioSrc.PlayOneShot(BgMusic);
    }
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "SoraJump":
                audioSrc.PlayOneShot(SoraJumpSound);
                audioSrc.PlayOneShot(SoraJump1);
                break;
            case "SoraAttack1":
                audioSrc.PlayOneShot(SoraAttackSound1);
                break;
            case "SoraAttack2":
                audioSrc.PlayOneShot(SoraAttackSound2);
                break;
            case "SoraAttack3":
                audioSrc.PlayOneShot(SoraAttackSound3);
                break;
            case "Hurt1":
                audioSrc.PlayOneShot(Hurt1);
                break;
            case "Hurt2":
                audioSrc.PlayOneShot(Hurt2);
                break;
            case "Hurt3":
                audioSrc.PlayOneShot(Hurt3);
                break;
            case "GokuKame":
                audioSrc.PlayOneShot(GokuKame);
                break;
            case "GokuJump1":
                audioSrc.PlayOneShot(GokuJump1);
                break;
            case "GokuAttack1":
                audioSrc.PlayOneShot(GokuAttack1);
                break;

        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
