using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform[] targets;
    public Vector3 offset, movePosition;
    public float damping;
    int players;
    private float X_max, X_min, Y_max, Y_min, X_center, Y_center, distanceX, distanceY, Final_Distance;
    
    [SerializeField]
    private float min_distance;
    [SerializeField]
    private float max_distance;

    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
         players = targets.Length;
    }
    private void FixedUpdate()
    {
        
        X_max = targets[0].position.x;
        X_min = targets[0].position.x;
        Y_max = targets[0].position.y;
        Y_min = targets[0].position.y;
        
        for (int i = 1; i < players; i++)
        {
            if (targets[i].position.x > X_max)
            {
                X_max = targets[i].position.x;
            }
            if (targets[i].position.x  < X_min)
            {
                X_min = targets[i].position.x ;
            }
            if (targets[i].position.y + offset.y > Y_max)
            {
                Y_max = targets[i].position.y + offset.y;
            }
            if (targets[i].position.y + offset.y < Y_min)
            {
                Y_min = targets[i].position.y + offset.y;
            }
        }
        
        X_center = (X_max + X_min) / 2;
        Y_center = (Y_max + Y_min) / 2;

        distanceX = (X_max - X_min) / players;
        distanceY = (Y_max - Y_min) / players;
        if (distanceX < min_distance && distanceY < min_distance)
        {
            distanceX = min_distance;
            
        }
        else if (distanceX > max_distance || distanceY > max_distance)
        {
            distanceX = max_distance;
            
        }
        else if (distanceY > distanceX)
        {
            distanceX = distanceY+1.5f;
        }
        
        this.transform.GetComponent<Camera>().orthographicSize = distanceX;
        
        movePosition = new Vector3(X_center,Y_center,0) + offset;
        if (movePosition.x+distanceX > 17f)
            movePosition.x = 17f-distanceX;
        else if (movePosition.x- distanceX < -17f)
            movePosition.x = -17f+distanceX;
        if (movePosition.y < -8f)
            movePosition.y = -8f;
        else if (movePosition.y > 6f)
            movePosition.y = 6f;
        transform.position = Vector3.SmoothDamp(transform.position, movePosition, ref velocity, damping);

        /*if (movePosition.x > 17f)
            movePosition.x = 17f;
        else if (movePosition.x < -17f)
            movePosition.x = -17f;
        if (movePosition.y < -6f)
            movePosition.y = -6f;
        else if(movePosition.y > 6f)
            movePosition.y = 6f;
        transform.position = Vector3.SmoothDamp(transform.position, movePosition,ref velocity, damping); */
        
    }


}
