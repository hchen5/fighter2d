using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GokuIdle : State
{
    GokuBehaviour m_GokuBehaviour;
    string m_IdleAnimations;
    
    public GokuIdle(FSM fSM, string idleAnimations)
        :base(fSM)
    {
        m_GokuBehaviour = fSM.Owner.GetComponent<GokuBehaviour>();
        m_IdleAnimations = idleAnimations;
    }

    public override void Init()
    {
        base.Init();
        
        m_GokuBehaviour.Actions.Land.Jump2.performed += StartedJumping;
        m_FSM.Owner.GetComponent<Animator>().Play(m_IdleAnimations);
        m_GokuBehaviour.Actions.Land.Atack.performed +=Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed += Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed += Kame;
        m_GokuBehaviour.Actions.Land.Cubrir.performed += Cubrir;
    }
    public override void Exit()
    {
        base.Exit();
        m_GokuBehaviour.Actions.Land.Jump2.performed -= StartedJumping;
        m_GokuBehaviour.Actions.Land.Atack.performed -= Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed -= Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed -= Kame;
        m_GokuBehaviour.Actions.Land.Cubrir.performed -= Cubrir;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_GokuBehaviour.Actions.Land.Movement2.ReadValue<float>() != 0)
            m_FSM.ChangeState<GokuRun>();

    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuJump>();

        
    }
    private void Punch01(InputAction.CallbackContext context) 
    {
        m_FSM.ChangeState<GokuPunch01>();
    }
    private void Punch02(InputAction.CallbackContext context) 
    {
        m_FSM.ChangeState<GokuPunch02>();
    }
    private void Patada(InputAction.CallbackContext context) 
    {
        m_FSM.ChangeState<GokuPunch03>();
    }
    private void Kame(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuKame>();
    }
    private void Cubrir(InputAction.CallbackContext context) 
    {
        m_FSM.ChangeState<GokuCubrir>();
    
    }
   
}
