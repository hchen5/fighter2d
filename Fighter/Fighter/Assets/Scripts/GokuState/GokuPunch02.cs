using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GokuPunch02 : Atack
{

    public GokuPunch02(FSM fSM, string animation, float comboTime, float stateLength)
        : base(fSM, animation, comboTime, stateLength)
    {

    }
    public override void Init()
    {
        base.Init();
       
    }
    public override void Exit()
    {
        base.Exit();
       
    }
    public override void Update()
    {
        base.Update();
        if (m_StateDelta >= m_ComboTime)
        {
            if (m_GokuBehaviour.Actions.Land.Atack.inProgress)
            {
                m_FSM.ChangeState<GokuPunch03>();
                return;
            }
        }

        if (m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<GokuIdle>();
            return;
        }
    }
}
