using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GokuFall : GokuMovement
{
    float m_MinimumSpeed;
    float m_JumpForce;
    
    public GokuFall(FSM fSM,string movementAnimation, float movementSpeed, float jumpForce) 
        : base(fSM,movementAnimation,movementSpeed)
    {
        m_MinimumSpeed = movementSpeed;
        m_JumpForce = jumpForce;
    }
    public override void Init()
    {
        base.Init();

        if (Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
            m_MovementSpeed = Mathf.Abs(m_RigidBody.velocity.x);
        else
            m_MovementSpeed = m_MinimumSpeed;


        m_GokuBehaviour.Actions.Land.Atack.performed += Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed += Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed += Kame;
        //m_GokuBehaviour.Actions.Land.Punch2.performed += Patada;
    }

    public override void Exit()
    {
        base.Exit();
        m_GokuBehaviour.Actions.Land.Atack.performed -= Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed -= Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed -= Kame;
        //m_GokuBehaviour.Actions.Land.Cubrir.performed -= Cubrir;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (m_RigidBody.velocity.y >= 0 && m_GokuBehaviour.CheckGround().collider != null)
        {

            if (m_GokuBehaviour.Actions.Land.Movement2.ReadValue<float>() != 0)
            {
                m_FSM.ChangeState<GokuRun>();
            }
            else
            {
                m_FSM.ChangeState<GokuIdle>();
            }
            
        } 
       
    }

    private void Punch01(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuPunch01>();

    }
    private void Punch02(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuPunch02>();
    }
    private void Patada(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuPunch03>();
    }
    private void Kame(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuKame>();
    }
    private void Cubrir(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuCubrir>();

    }




}
