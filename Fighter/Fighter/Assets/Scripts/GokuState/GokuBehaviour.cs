
using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GokuBehaviour : MonoBehaviour
{
    FSM m_FSM;
    PlayerControls m_Actions;
    public PlayerControls Actions => m_Actions;

    [SerializeField]
    private LayerMask m_LayerMask;

    
    [SerializeField]
    private float m_RunSpeed=1f;
    [SerializeField]
    private float m_JumpForce = 1f;

    private void Awake()
    {
        m_Actions = new PlayerControls();
        m_Actions.Land.Enable();

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new GokuIdle(m_FSM, "GokuIdle"));
        m_FSM.AddState(new GokuRun(m_FSM,"GokuRun",m_RunSpeed));
        m_FSM.AddState(new GokuJump(m_FSM, "SaltarGoku", m_RunSpeed, m_JumpForce));
        m_FSM.AddState(new GokuFall(m_FSM,"CaerSueloGoku",m_RunSpeed,m_JumpForce));
        m_FSM.AddState(new GokuPunch01(m_FSM, "Punch01Goku", 0.8f, 1.1f));
        m_FSM.AddState(new GokuPunch02(m_FSM,"Punch02Goku",0.8f,1.1f));
        m_FSM.AddState(new GokuPunch03(m_FSM, "PatadaGoku", 0.88f, 1.1f));
        m_FSM.AddState(new GokuKame(m_FSM,"KameHameGoku",2f,3f));
        m_FSM.AddState(new GokuCubrir(m_FSM,"GokuCubrir",1f));
        //m_FSM.AddState(new GokuJump2(m_FSM,"GokuJump2",m_RunSpeed,m_JumpForce));


        m_FSM.ChangeState<GokuIdle>();
    }
    public RaycastHit2D CheckGround()
    {

        return Physics2D.Raycast(transform.position, Vector2.down, (GetComponent<BoxCollider2D>().size.y / 2) * 1.1f, m_LayerMask);
       
    }

    void Update()
    {
        m_FSM.Update();
    }

    void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        m_FSM.OnCollisionEnter2D(collision);
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        m_FSM.OnCollisionStay2D(collision);
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        m_FSM.OnCollisionExit2D(collision);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        m_FSM.OnTriggerEnter2D(collider);
    }
    void OnTriggerStay2D(Collider2D collider)
    {
        m_FSM.OnTriggerStay2D(collider);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        m_FSM.OnTriggerExit2D(collider);
    }


}
