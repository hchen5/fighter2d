using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class GokuMovement : State
{
    protected GokuBehaviour m_GokuBehaviour;
    protected Rigidbody2D m_RigidBody;
    protected SpriteRenderer m_SpriteRenderer;
    protected string m_MovementAnimation;
    protected float m_MovementSpeed;
   
    public GokuMovement(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM)
    {
        m_GokuBehaviour = fSM.Owner.GetComponent<GokuBehaviour>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_SpriteRenderer = fSM.Owner.GetComponent<SpriteRenderer>();
        m_MovementAnimation = movementAnimation;
        m_MovementSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();
        m_GokuBehaviour.Actions.Land.Movement2.canceled += StopMovement;
        
            m_GokuBehaviour.Actions.Land.Jump2.performed += StartedJumping;
        
        m_FSM.Owner.GetComponent<Animator>().Play(m_MovementAnimation);
    }
    public override void Exit()
    {
        base.Exit();
        m_GokuBehaviour.Actions.Land.Movement2.canceled -= StopMovement;
        m_GokuBehaviour.Actions.Land.Jump2.performed -= StartedJumping;
    }

    private void StopMovement(InputAction.CallbackContext context)
    {
        m_RigidBody.velocity = new Vector2(0, m_RigidBody.velocity.y);
        if (m_RigidBody.velocity.y == 0 )
            m_FSM.ChangeState<GokuIdle>();
        //else
           // m_FSM.ChangeState<GokuJump>();

    }
    
   
    private void StartedJumping(InputAction.CallbackContext context)
    {
       
            m_FSM.ChangeState<GokuJump>();
        
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        float horizontalAxis = m_GokuBehaviour.Actions.Land.Movement2.ReadValue<float>();
        if (horizontalAxis != 0f)
        {
            if (horizontalAxis > 0)
            {
                m_RigidBody.velocity = new Vector2(m_MovementSpeed, m_RigidBody.velocity.y);
                //m_SpriteRenderer.flipX = false;
                m_RigidBody.transform.eulerAngles = new Vector2(0, 0);
            }
            else
            {
                m_RigidBody.velocity = new Vector2(-m_MovementSpeed, m_RigidBody.velocity.y);
                //m_SpriteRenderer.flipX = true;
                m_RigidBody.transform.eulerAngles = new Vector2(0, 180);
            }
        }
       
    }
}
