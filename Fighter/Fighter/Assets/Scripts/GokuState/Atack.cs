using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atack : State
{
    protected GokuBehaviour m_GokuBehaviour;
    protected string m_Animation;
    protected float m_ComboTime;
    protected float m_StateDelta;
    protected float m_StateLength;
    protected Rigidbody2D m_RigidBody;

    public Atack(FSM fSM, string animation, float comboTime, float stateLength) 
        :base(fSM)
    {
        m_GokuBehaviour = fSM.Owner.GetComponent<GokuBehaviour>();
        m_Animation = animation;
        m_ComboTime = comboTime;
        m_StateLength = stateLength;
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody2D>();
    }
    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
        SoundManagerScript.PlaySound("GokuAttack1");
        if (m_GokuBehaviour.gameObject.transform.rotation.y == 0)
            m_RigidBody.AddForce(new Vector2(1f, 0), ForceMode2D.Impulse);
        else
            m_RigidBody.AddForce(new Vector2(-1f, 0), ForceMode2D.Impulse);
    }
    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;

    }

    
}
