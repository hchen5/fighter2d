using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GokuCubrir : State
{
    protected GokuBehaviour m_GokuBehaviour;
    protected string m_Animation;
    protected float m_StateDelta;
    protected float m_StateLength;


    public GokuCubrir(FSM fSM, string animation, float stateLength)
         : base(fSM)
    {
        m_GokuBehaviour = fSM.Owner.GetComponent<GokuBehaviour>();
        m_Animation = animation;
        m_StateLength = stateLength;
    }
    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
    }
    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;
        if (m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<GokuIdle>();
            return;
        }
    }

   
}
