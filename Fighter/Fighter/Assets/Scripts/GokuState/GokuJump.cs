using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class GokuJump : GokuMovement
{
    float m_JumpForce;
    float m_MinimumSpeed;
    //Coroutine m_FallingCorutine;
    public GokuJump(FSM fSM, string movementAnimation, float movementSpeed, float jumpForce)
       : base(fSM, movementAnimation, movementSpeed)
    {
        m_JumpForce = jumpForce;
        m_MinimumSpeed = movementSpeed;
        
    }
    public override void Init()
    {
        base.Init();

        if (Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
            m_MovementSpeed = Mathf.Abs(m_RigidBody.velocity.x);
        else
            m_MovementSpeed = m_MinimumSpeed;

        //if (m_RigidBody.velocity.y == 0)
            m_RigidBody.AddForce(Vector2.up * m_JumpForce, ForceMode2D.Impulse);
        SoundManagerScript.PlaySound("GokuJump1");
        //Debug.Log("JumpForce: "+m_JumpForce+" Velocity: "+m_RigidBody.velocity.y);
        //m_FallingCorutine = 
         m_GokuBehaviour.StartCoroutine(StartFalling());

       
    }
    public override void Exit()
    {
        base.Exit();
        
       
    }

    private IEnumerator StartFalling()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        m_FSM.ChangeState<GokuFall>();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_RigidBody.velocity.y < 0 && m_GokuBehaviour.CheckGround().collider != null)
        {
            if (m_GokuBehaviour.Actions.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<GokuRun>();
            else
                m_FSM.ChangeState<GokuIdle>();
        }
       
    }
    

}
