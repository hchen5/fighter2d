using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GokuKame : Atack
{
    public GokuKame(FSM fSM, string animation, float comboTime, float stateLength)
        : base(fSM, animation, comboTime, stateLength)
    {

    }
    public override void Init()
    {
        base.Init();
        SoundManagerScript.PlaySound("GokuKame");
    }
    public override void Exit()
    {
        base.Exit();

    }

    public override void Update()
    {
        base.Update();

        /*if (m_StateDelta >= m_ComboTime)
        {
            if (m_GokuBehaviour.Actions.Land.Atack.inProgress)
            {
                m_FSM.ChangeState<GokuPunch02>();
                return;
            }
        }*/

        if (m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<GokuIdle>();
            return;
        }
    }
   
}
