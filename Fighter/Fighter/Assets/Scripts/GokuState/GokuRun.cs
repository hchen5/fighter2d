using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GokuRun : GokuMovement
{
    public GokuRun(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM, movementAnimation, movementSpeed)
    {
    }
    public override void Init()
    {
        base.Init();
        m_GokuBehaviour.Actions.Land.Atack.performed += Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed += Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed += Kame;
        m_GokuBehaviour.Actions.Land.Punch2.performed += Patada;
        //m_GokuBehaviour.Actions.Land.Punch2.performed += Cubrir;
    }
    public override void Exit()
    {
        base.Exit();
        m_GokuBehaviour.Actions.Land.Atack.performed -= Punch01;
        m_GokuBehaviour.Actions.Land.Punch2.performed -= Punch02;
        m_GokuBehaviour.Actions.Land.Kame.performed -= Kame;
        m_GokuBehaviour.Actions.Land.Punch2.performed -= Patada;
        //m_GokuBehaviour.Actions.Land.Punch2.performed -= Cubrir;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_RigidBody.velocity.y < 0 && m_GokuBehaviour.CheckGround().collider == null)
        {
            m_FSM.ChangeState<GokuFall>();
        }
    }

    private void Punch01(InputAction.CallbackContext context) 
    {
        m_FSM.ChangeState<GokuPunch01>();
    
    }
    private void Punch02(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuPunch02>();
    }
    private void Patada(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuPunch03>();
    }
    private void Kame(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuKame>();
    }
    private void Cubrir(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<GokuCubrir>();

    }

}
