using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Rendering.LookDev;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoraBehaviour : MonoBehaviour
{
    private FSM m_FSM;
    
    PlayerControls m_PlayerControls;
    public PlayerControls Controls => m_PlayerControls;

    [SerializeField]
    GameObject KeyProyectil;

    [SerializeField]
    private LayerMask m_LayerMask;

    [SerializeField]
    private float m_Speed = 3;
    [SerializeField]
    private float m_Jump = 5;

    void Awake()
    {
        m_PlayerControls = new PlayerControls();
        m_PlayerControls.Land.Enable();

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new SoraIdle(m_FSM,"IdleSora"));
        m_FSM.AddState(new SoraRun(m_FSM, "RunningSoraTemp", m_Speed));
        m_FSM.AddState(new SoraJump1(m_FSM, "FirstJumpSora", m_Speed, m_Jump));
        m_FSM.AddState(new SoraFalling(m_FSM,"OnAirSora",m_Speed, m_Jump));
        m_FSM.AddState(new SoraJump2(m_FSM, "SecondJumpSora", m_Speed, m_Jump));
        m_FSM.AddState(new SoraJAB1(m_FSM,"JAB1Sora",0.6f,0.76f));
        m_FSM.AddState(new SoraJAB2(m_FSM, "JAB2Sora", 0.93f, 1.12f));
        m_FSM.AddState(new SoraJAB3(m_FSM, "JAB3Sora", 0.61f, 0.91f));
        m_FSM.AddState(new SoraThrowing(m_FSM, "ThrowKeySora"));
        m_FSM.ChangeState<SoraFalling>();
        
    }

    void Update()
    {
        m_FSM.Update();
    }
    public void ThrowKey()
    {
        KeyProyectil.SetActive(true);
        if (this.gameObject.transform.rotation.y == -1)
        {
            //mirando izq
            KeyProyectil.transform.position = this.transform.position - new Vector3(0.85f, 0, 0);
            KeyProyectil.GetComponent<SoraKeyBehaviour>().ToThrow(new Vector2(-6, 0));
        }
        else
        {
            //mirando der
            KeyProyectil.transform.position = this.transform.position + new Vector3(0.85f, 0, 0);
            KeyProyectil.GetComponent<SoraKeyBehaviour>().ToThrow(new Vector2(6, 0));
        }
    }
    public RaycastHit2D CheckGround()
    {
        //Debug.DrawLine(transform.position, Physics2D.Raycast(transform.position, Vector2.down, (GetComponent<BoxCollider2D>().size.y / 2) * 1.01f, m_LayerMask).point, Color.blue, 2f);
        return Physics2D.Raycast(transform.position, Vector2.down, (GetComponent<BoxCollider2D>().size.y/2) * 1.1f, m_LayerMask);

        
    }
    
    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        m_FSM.OnCollisionEnter2D(collision);
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        m_FSM.OnCollisionStay2D(collision);
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        m_FSM.OnCollisionExit2D(collision);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        m_FSM.OnTriggerEnter2D(collider);
        if (collider.gameObject == KeyProyectil)
            KeyProyectil.SetActive(false);
    }
    void OnTriggerStay2D(Collider2D collider)
    {
        m_FSM.OnTriggerStay2D(collider);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        m_FSM.OnTriggerExit2D(collider);
    }
}
