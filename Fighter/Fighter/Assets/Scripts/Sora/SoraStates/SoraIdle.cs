using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoraIdle : State
{
    SoraBehaviour m_SoraBehaviour;
    string m_IdleAnimation;

    public SoraIdle(FSM fSM, string idleAnimation)
        : base(fSM)
    {
        m_SoraBehaviour = fSM.Owner.GetComponent<SoraBehaviour>();
        m_IdleAnimation = idleAnimation;
        
    }

    public override void Init()
    {
        base.Init();
        //SoraMovement.m_Has1rstJump = true;
        //SoraMovement.m_Has2ndJump=true;
        m_SoraBehaviour.Controls.Land.Jump.performed += StartedJumping;
        m_FSM.Owner.GetComponent<Animator>().Play(m_IdleAnimation);
        m_SoraBehaviour.Controls.Land.Attack.performed += StartedAttacking;
        m_SoraBehaviour.Controls.Land.Attack1.performed += StartedThrowing;
    }

    public override void Exit()
    {
        base.Exit();
        m_SoraBehaviour.Controls.Land.Jump.performed -= StartedJumping;
        m_SoraBehaviour.Controls.Land.Attack.performed -= StartedAttacking;
        m_SoraBehaviour.Controls.Land.Attack1.performed -= StartedThrowing;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0)
            m_FSM.ChangeState<SoraRun>();

    }

    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SoraJump1>();
    }
    private void StartedThrowing(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SoraThrowing>();
    }
    private void StartedAttacking(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SoraJAB1>();
    }
}
