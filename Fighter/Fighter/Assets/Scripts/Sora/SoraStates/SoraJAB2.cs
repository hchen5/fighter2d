using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class SoraJAB2 : SoraAttacking
{

    public SoraJAB2(FSM fSM, string animation, float comboTime, float stateLength)
        : base(fSM, animation, comboTime, stateLength)
    {

    }

    public override void Init()
    {
        base.Init();
        SoundManagerScript.PlaySound("SoraAttack2");
    }
    public override void Update()
    {
        base.Update();
        if (m_StateDelta >= m_ComboTime)
        {
            if (m_SoraBehaviour.Controls.Land.Attack.inProgress)
            {
                m_FSM.ChangeState<SoraJAB3>();
                return;
            }
        }

        if (m_StateDelta >= m_StateLength)
        {
            if (m_RigidBody.velocity.y < 0 && m_SoraBehaviour.CheckGround().collider == null)
                m_FSM.ChangeState<SoraFalling>();
            
            m_FSM.ChangeState<SoraIdle>();
            return;
        }
    }
}

