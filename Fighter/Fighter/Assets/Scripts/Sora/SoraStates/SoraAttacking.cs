
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;

public abstract class SoraAttacking : State
{
    protected SoraBehaviour m_SoraBehaviour;
    protected string m_Animation;
    protected float m_ComboTime;
    protected float m_StateDelta;
    protected float m_StateLength;
    protected Rigidbody2D m_RigidBody;

    public SoraAttacking(FSM fSM, string animation, float comboTime, float stateLength)
        : base(fSM)
    {
        m_SoraBehaviour = fSM.Owner.GetComponent<SoraBehaviour>();
        m_Animation = animation;
        m_ComboTime = comboTime;
        m_StateLength = stateLength;
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody2D>();
    }

    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
        if (m_SoraBehaviour.gameObject.transform.rotation.y == 0)
            m_RigidBody.AddForce(new Vector2(2.66f, 0), ForceMode2D.Impulse);
        else
            m_RigidBody.AddForce(new Vector2(-2.66f, 0), ForceMode2D.Impulse);



    }
    public override void Exit()
    {
        base.Exit();
        
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;
        
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        /*if (m_RigidBody.velocity.y < 0 && m_SoraBehaviour.CheckGround().collider != null)
        {
            if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<SoraRun>();
            else
                m_FSM.ChangeState<SoraIdle>();

        }*/
    }

}


