using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoraJump2 : SoraMovement
{
    float m_JumpForce;
    float m_MinimumSpeed;
    Coroutine m_FallingCorutine;

    public SoraJump2(FSM fSM, string movementAnimation, float movementSpeed, float jumpForce)
        : base(fSM, movementAnimation, movementSpeed)
    {
        m_JumpForce = jumpForce;
        m_MinimumSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();

        if (Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
            m_MovementSpeed = Mathf.Abs(m_RigidBody.velocity.x);
        else
            m_MovementSpeed = m_MinimumSpeed;

        //if (m_RigidBody.velocity.y == 0)
        m_RigidBody.AddForce(Vector2.up * m_JumpForce, ForceMode2D.Impulse);

        m_FallingCorutine = m_SoraBehaviour.StartCoroutine(StartFalling());
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (m_RigidBody.velocity.y < 0 && m_SoraBehaviour.CheckGround().collider != null)
        {
            if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<SoraRun>();
            else
                m_FSM.ChangeState<SoraIdle>();

        }
    }

    public override void Exit()
    {
        base.Exit();

        m_SoraBehaviour.StopCoroutine(m_FallingCorutine);
    }

    private IEnumerator StartFalling()
    {
        yield return new WaitForSecondsRealtime(0.9f);
        m_FSM.ChangeState<SoraFalling>();
    }
}
