using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;
using System;

public abstract class SoraMovement : State
{
    protected SoraBehaviour m_SoraBehaviour;
    protected Rigidbody2D m_RigidBody;
    protected SpriteRenderer m_SpriteRenderer;
    protected string m_MovementAnimation;
    protected float m_MovementSpeed;
    //static public Boolean m_Has1rstJump;
    //static public Boolean m_Has2ndJump;

    public SoraMovement(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM)
    {
        m_SoraBehaviour = fSM.Owner.GetComponent<SoraBehaviour>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_SpriteRenderer = fSM.Owner.GetComponent<SpriteRenderer>();
        m_MovementAnimation = movementAnimation;
        m_MovementSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();
        m_SoraBehaviour.Controls.Land.Movement.canceled += StopMovement;
        m_SoraBehaviour.Controls.Land.Jump.performed += StartedJumping;
        m_FSM.Owner.GetComponent<Animator>().Play(m_MovementAnimation);
    }

    public override void Exit()
    {
        base.Exit();
        m_SoraBehaviour.Controls.Land.Movement.canceled -= StopMovement;
        m_SoraBehaviour.Controls.Land.Jump.performed -= StartedJumping;
       
    }

    private void StopMovement(InputAction.CallbackContext context)
    {
        //Debug.Log("ha entrado en stopmovement");//Debug.Log(m_RigidBody.velocity.y);
        m_RigidBody.velocity = new Vector2(0, m_RigidBody.velocity.y);
        
        if (m_RigidBody.velocity.y == 0f)
            m_FSM.ChangeState<SoraIdle>();

    }


   


    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SoraJump1>();
        /*if (SoraMovement.m_Has1rstJump)
        {
            m_FSM.ChangeState<SoraJump1>();
            SoraMovement.m_Has1rstJump = false;
        }else if (SoraMovement.m_Has2ndJump)
        {
            m_FSM.ChangeState<SoraJump2>();
            SoraMovement.m_Has2ndJump = false;
        }*/
        
        
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        float horizontalAxis = m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>();
        if (horizontalAxis != 0f)
        {
            if (horizontalAxis > 0)
            {
                m_RigidBody.velocity = new Vector2(m_MovementSpeed, m_RigidBody.velocity.y);
               // m_SpriteRenderer.flipX = false;
                m_RigidBody.transform.eulerAngles = new Vector2(0, 0);
            }
            else
            {
                m_RigidBody.velocity = new Vector2(-m_MovementSpeed, m_RigidBody.velocity.y);
                //m_SpriteRenderer.flipX = true;
                m_RigidBody.transform.eulerAngles = new Vector2(0, 180);
            }
        }
    }
}
