using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class SoraThrowing : State
{
    SoraBehaviour m_SoraBehaviour;
    string m_animation;
    float m_StateDelta;
    float miniumTime = 2.15f;
    public SoraThrowing(FSM fSM, string mAnimation)
       : base(fSM)
    {
        m_SoraBehaviour = fSM.Owner.GetComponent<SoraBehaviour>();
        m_animation = mAnimation;

    }
    public override void Init()
    {
        base.Init();
        m_StateDelta = 0;
        m_FSM.Owner.GetComponent<Animator>().Play(m_animation);
    }
    public override void Update()
    {
        base.Update();
        if (m_StateDelta >= 2.45f)
        {
            m_FSM.ChangeState<SoraIdle>();
            return;
        }
        m_StateDelta += Time.deltaTime;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0 && m_StateDelta>= miniumTime)
            m_FSM.ChangeState<SoraRun>();

        if (m_StateDelta >= miniumTime)
        {
            if (m_SoraBehaviour.Controls.Land.Attack.inProgress)
            {
                m_FSM.ChangeState<SoraJAB1>();
                return;
            }
        }
        //2.48f
    }

}
