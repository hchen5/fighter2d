using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoraRun : SoraMovement
{
    public SoraRun(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM, movementAnimation, movementSpeed)
    {
        
    }
    public override void Init()
    {
        base.Init();
        m_SoraBehaviour.Controls.Land.Attack.performed += StartedAttacking;
        //SoraMovement.m_Has1rstJump = true;
        //SoraMovement.m_Has2ndJump = true;
    }
    public override void Exit()
    {
        base.Exit();
        m_SoraBehaviour.Controls.Land.Attack.performed -= StartedAttacking;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_RigidBody.velocity.y < 0 && m_SoraBehaviour.CheckGround().collider == null)
        {
                m_FSM.ChangeState<SoraFalling>();
        }
    }

    private void StartedAttacking(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SoraJAB1>();
    }

}
