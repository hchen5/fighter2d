using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoraLanding : SoraMovement
{
  
    public SoraLanding(FSM fSM, string movementAnimation, float movementSpeed, float jumpForce)
        : base(fSM, movementAnimation, movementSpeed)
    {
  
    }
    public override void Init()
    {
        base.Init();

        
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();

        if (m_RigidBody.velocity.y >= 0 && m_SoraBehaviour.CheckGround().collider != null)
        {

            if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<SoraRun>();
            else
                m_FSM.ChangeState<SoraIdle>();

        }

    }
}
