using FiniteStateMachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoraFalling : SoraMovement
{
    float m_MinimumSpeed;
    float m_JumpForce;
    public SoraFalling(FSM fSM, string movementAnimation, float movementSpeed, float jumpForce)
        : base(fSM, movementAnimation, movementSpeed)
    {
        m_MinimumSpeed = movementSpeed;
        m_JumpForce = jumpForce;
    }
    public override void Init()
    {
        base.Init();

        if (Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
            m_MovementSpeed = Mathf.Abs(m_RigidBody.velocity.x);
        else
            m_MovementSpeed = m_MinimumSpeed;
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_SoraBehaviour.Controls.Land.Attack.inProgress)
        {
            m_FSM.ChangeState<SoraJAB1>();
            return;
        }
        else if (m_RigidBody.velocity.y >= 0 && m_SoraBehaviour.CheckGround().collider != null)
        {

            if (m_SoraBehaviour.Controls.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<SoraRun>();
            else
                m_FSM.ChangeState<SoraIdle>();

        }

    }
}
