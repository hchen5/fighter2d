using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Rendering.LookDev;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoraBEmpty : MonoBehaviour
{


    
    Rigidbody2D m_RigidBody;

    string[] hurt = new string[] { "Hurt1", "Hurt2", "Hurt3" };

    [SerializeField]
    string m_tag;

    Vector3 posit;

    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
       
    }

    
    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("MY TAG-> " + m_tag + " / COLLIDER TAG-> " + collider.tag);
        if (collider.tag != m_tag && (collider.name == "Hitbox" || collider.name == "hitbox" || collider.name == "SoraKeyP1"|| collider.name == "Kame"))
        {

            
            if(collider.name== "SoraKeyP1")
                posit = collider.transform.position;
            else
                posit = collider.transform.parent.gameObject.transform.position;
            SoundManagerScript.PlaySound(hurt[Random.Range(0, 2)]);
            if (this.transform.position.x > posit.x)
                m_RigidBody.AddForce(new Vector2(2, 2), ForceMode2D.Impulse);
            else
                m_RigidBody.AddForce(new Vector2(-2, 2), ForceMode2D.Impulse);
        }
        
            
    }
    
    

}
