using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoraKeyBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject m_Sora;

    Rigidbody2D m_RigidBody;
    float timer = 0.75f;
    int state;
    Vector2 Direction;
    //1 throwed //2 returning
   
    void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        state = 0;
    }

    void Update()
    {
        if (state == 2)
        {
            Direction = new Vector2(
                 m_Sora.transform.position.x - this.transform.position.x,
                 m_Sora.transform.position.y - this.transform.position.y
                 );

            this.m_RigidBody.velocity = Direction.normalized * 5;

        }
    }
    public void ToThrow(Vector2 Direccio)
    {
        this.m_RigidBody.velocity = Direccio;
        timer = 0.75f;
        state = 1;
        StartCoroutine(Timer());
    }
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject != m_Sora)
            state = 2;
    }
    public IEnumerator Timer()
    {
        yield return new WaitForSeconds(timer);
        state = 2;
    }
}
