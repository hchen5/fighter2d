using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitboxhandler : MonoBehaviour
{
    public int Damage;
    [SerializeField]
    private LayerMask collisionLayer;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        print(gameObject + " : " + collision.gameObject);

        if (collision.gameObject.tag == "Player")
        {
            int damage = collision.gameObject.GetComponent<Hitboxhandler>().Damage;
            print("Rebo " + damage + " punts de mal.");
        }
    }

}
