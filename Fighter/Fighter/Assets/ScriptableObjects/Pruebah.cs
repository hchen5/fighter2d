
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pruebah : MonoBehaviour
{
    [SerializeField]
    private float m_Speed = 3;
    [SerializeField]
    private float m_Jump = 5;

    private Rigidbody2D m_Rigidbody2D;

    void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            m_Rigidbody2D.velocity = Vector2.right * m_Speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            m_Rigidbody2D.velocity = Vector2.left * m_Speed;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Rigidbody2D.velocity += Vector2.up * m_Jump;
        }

        CheckGround();
    }


    [SerializeField]
    LayerMask m_LayerMask;
    private void CheckGround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up, 3f, m_LayerMask);
        if (hit.collider != null)
        {
            Debug.DrawLine(transform.position, hit.point, new Color(1, 0, 1));
        }
    }
}



