using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "ElMeuScriptable", menuName = "ScriptableObjects/ElMeuScriptable")]
public class ElMeuScriptable : ScriptableObject
{
    public int m_VidaMax;
    public int ValorActual;
    
}